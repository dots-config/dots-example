# rbenv installation

installs [rbenv](https://github.com/rbenv/rbenv) and adds it to path

## Usage

After installing rbenv you will manually need to install ruby

```bash
# list all available versions:
rbenv install -l

# install a Ruby version:
rbenv install 2.7.1
```

### Dependencies needed

```bash
sudo apt-get install -y libssl-dev zlib1g-dev
```
