# load zplug
. ~/.zplug/init.zsh

# zsh-users
zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "zsh-users/zsh-history-substring-search", defer:3


zplug "woefe/wbase.zsh"
zplug "zpm-zsh/autoenv"
zplug "zpm-zsh/colors"
zplug "mafredri/zsh-async"

# oh-my-zsh
zplug "plugins/sudo", from:oh-my-zsh
zplug "plugins/chucknorris", from:oh-my-zsh
#zplug "plugins/git",   from:oh-my-zsh

# prezto
zplug "modules/utility", from:prezto
zplug "modules/ssh", from:prezto
zplug "modules/git", from:prezto
zplug "modules/directory", from:prezto
zplug "modules/command-not-found", from:prezto

# Load theme file
#zplug 'dracula/zsh', as:theme
#zplug sindresorhus/pure, use:pure.zsh, from:github, as:theme
zplug "spaceship-prompt/spaceship-prompt", use:spaceship.zsh, from:github, as:theme

#zplug 'zplug/zplug', hook-build:'zplug --self-manage'

if ! zplug check --verbose; then
    zplug install
fi

zplug load

[ -f ~/.sourcerc ] && . ~/.sourcerc

export EDITOR=vim
