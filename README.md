# dots example config

Some example packages to be used with [dots](https://gitlab.com/kempe/dots)

## Installation

### Install dots

```bash
curl -sSLo dots https://gitlab.com/kempe/dots/raw/master/dots
chmod +x dots
mv dots /usr/local/bin
```

### Get example files

The repo includes submodules so it needs to be cloned with the recursive flag

```bash
git clone --recursive https://gitlab.com/dots-config/dots-example ~/.dots
cd ~/.dots
```

## Usage

Packages can have dependencies on other packages and a bootstrap script that installs other applications

### Show help

```bash
dots -h
```

### Example 1

To install git, vim and zsh

```bash
dots git vim zsh
```

__zsh__ has a dependency on __source__ so the acctual packages and order  would be __git__, __vim__, __source__, __zsh__

### Example 2

To install and bootstrap osx

```bash
dots osx
```

Would install __source__, __bash__, __git__, __vim__, __weechat__, __zsh__, __powerfonts__, _(and it's dependencies)_

## source

Source is a helper script for sourcing files, it looks for source files in __~/.source.d/__

* It will automagically source anything with the suffix __.autosource__ in that directory
* And source any __.source__ file in that directory by name (i.e. if there is a file called ubuntu-helpers in that directory it can be sourced by typing `source ubuntu-helpers` wherever you are)

### Configuration

sourcerc needs to be sourced before it can be used. _It is already included in the zsh and bash example packages_

```bash
[ -f ~/.sourcerc ] && . ~/.sourcerc
```
