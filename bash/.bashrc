# .bashrc

# Source global definitions
[ -f /etc/bashrc ] && . /etc/bashrc

# Source sourcerc
[ -f ~/.sourcerc ] && . ~/.sourcerc

# Source all files in bash.d
for file in $(find ~/.bash.d/ -type f); do 
	. $file;
done

# User specific environment and startup programs
export PATH=$PATH:$HOME/.local/bin:$HOME/bin
