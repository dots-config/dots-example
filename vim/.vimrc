"=======================
" General
"=======================

set nocompatible

" Syntax highlighting
syntax enable

" Enable filetype plugins
filetype plugin on
filetype indent on

" Sets how many lines of history VIM has to remember
set history=100

" sudo save
cmap w!! w !sudo tee >/dev/null %

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


"=======================
" UI related
"=======================

" color and  theme
set background=dark
colorscheme desert

" Set default font
set guifont="Inconsolata-dz for Powerline":h12

" Highlight current row
set cursorline

" Use visualbell instead of sound 
set visualbell

" scroll window when 10 rows/cols from end/begining
set scrolloff=10
set sidescrolloff=10

" Enable line numbers
set number


" Tabs
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set autoindent
set smartindent
set smarttab

" redraw only when we need to.
set lazyredraw


"=======================
" Search
"=======================

" Case-insensitive search if all small
set ignorecase
set smartcase

" Highlight search
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Clear highlight on dubble esc
nnoremap <esc><esc> :noh<return>


" Wildmenu
set wildmenu
set wildcharm=<TAB>

" Backups
set nobackup
set nowritebackup
set noswapfile

" Folding
set foldlevelstart=99
set nofoldenable
nmap <space> za

" Mouse
set mouse=a

" Auto reload vimrc on save
augroup myvimrc
	au!
	au BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $MYVIMRC | if has('gui_running') | so $MYGVIMRC | endif
augroup END

" Plugins
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline'
Plug 'airblade/vim-gitgutter'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

colorscheme dracula

map <C-n> :NERDTreeToggle<CR>
